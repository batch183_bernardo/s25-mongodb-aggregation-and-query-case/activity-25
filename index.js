
// Activity # 2

db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {"_id": "$onSale", "fruitsOnSale": {$count: {}}}},
	{$project: {"_id": 0}}
]);

// Activity # 3

db.fruits.aggregate([
	{$match: {"stocks": {$gte: 20} }},
	{$group: {"_id": "$onSale", "enoughStock": {$count: {}}}},
	{$project: {"_id": 0}}
]);

// Activity # 4

db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {"_id": "$supplier_id", "avg_price": {$avg: "$price" }}},
	{$sort: {"avg_price": -1}}
]);

// Activity # 5

db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {"_id": "$supplier_id", "max_price": {$max: "$price" }}}
]);

//Activity $ 6

db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {"_id": "$supplier_id", "min_price": {$min: "$price" }}},
	{$sort: {"min_price": -1}}
]);